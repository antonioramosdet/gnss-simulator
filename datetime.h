//
// Created by javier on 3/11/15.
//

#ifndef GNSS_SIM_DATETIME_H
#define GNSS_SIM_DATETIME_H

#include "gpstime.h"

class datetime {
public:
    int y; 		/*!< Calendar year */
    int m;		/*!< Calendar month */
    int d;		/*!< Calendar day */
    int hh;		/*!< Calendar hour */
    int mm;		/*!< Calendar minutes */
    double sec;	/*!< Calendar seconds */

    void jd2cal(const double jd, int *y, int *m, double *day_fraq);
    void gps2date(gpstime *g);

    /*! \brief Convert a UTC date into a GPS date
    *  \param[in] t input date in UTC form
    *  \param[out] g output date in GPS form
    */
    gpstime date2gps();
    datetime();
};


#endif //GNSS_SIM_DATETIME_H
