//
// Created by javier on 3/11/15.
//

#ifndef GNSS_SIM_SIMULATOR_CONTROL_H
#define GNSS_SIM_SIMULATOR_CONTROL_H
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <boost/thread.hpp>
#include "stdio.h"
#include "gpstime.h"
#include "gps_ephemeris.h"
#include "simple_rinex2_writer.h"
#include "tracking_obs_writer.h"
#include "gps_l1_ca_sim_channel_cc.h"
#include "interfaces/configuration_interface.h"
#include "concurrent_map.h"


class simulator_control {

public:
    bool open_out_file(std::string out_file);
    bool open_rinex_obs_file(std::string obs_file);
    /*! \brief Read Ephemersi data from the RINEX Navigation file */
    bool read_eph(std::string eph_file);
    simulator_control();
    ~simulator_control();

    /*! \brief Read the list of user motions from the input file
    *  \param[out] xyz Output array of ECEF vectors for user motion
    *  \param[[in] filename File name of the text input file
    *  \returns Number of user data motion records read, -1 on error
    */
    int readUserMotion(std::string &filename);
    int readNmeaGGA(std::string &filename);
    bool setsamplingfreq(double freq);
    void setCN0(double CN0_dBHz);
    void setstaticposition(double lat, double lon, double h, unsigned int num_points);
    bool run();

private:

    std::shared_ptr<ConfigurationInterface> configuration_;
    simple_rinex2_writer rinex_obs_writer;
    std::vector<std::shared_ptr<tracking_obs_writer>> trk_obs_writer_vec;
    std::vector<std::shared_ptr<gps_l1_ca_sim_channel_cc>> d_gps_l1_ca_channels_vec;
    std::vector<std::vector<double>> d_obs_xyz;
    concurrent_map<Gps_Ephemeris> d_gps_eph_map;
    double d_samp_freq;
    double d_signal_power_lin;
    double d_signal_amplitude_lin;
    double d_CN0_dBHz;
    bool d_enable_noise;
    std::string d_out_filename;
    std::ofstream d_out_filestream;

    int replaceExpDesignator(char *str, int len);
};


#endif //GNSS_SIM_SIMULATOR_CONTROL_H
