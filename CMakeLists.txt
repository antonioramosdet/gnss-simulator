# Copyright (C) 2010-2015 (see AUTHORS file for a list of contributors)
#
# This file is part of GNSS-SDR.
#
# GNSS-SDR is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GNSS-SDR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNSS-SDR. If not, see <http://www.gnu.org/licenses/>.
#


########################################################################
# Project setup
########################################################################
if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message(WARNING "In-tree build is bad practice. Try 'cd build && cmake ../' ")
endif(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
cmake_minimum_required(VERSION 2.8)
project(gnss_sim CXX C)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/Modules)
file(RELATIVE_PATH RELATIVE_CMAKE_CALL ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/Modules)

option(ENABLE_OWN_GLOG "Download glog and link it to gflags" OFF)
option(ENABLE_LOG "Enable logging" ON)

########################################################################
# Environment setup
########################################################################
include(ExternalProject)

################################################################################
# Boost - http://www.boost.org
################################################################################
if(UNIX AND EXISTS "/usr/lib64")
    list(APPEND BOOST_LIBRARYDIR "/usr/lib64") # Fedora 64-bit fix
endif(UNIX AND EXISTS "/usr/lib64")
if(GNURADIO_INSTALL_PREFIX)
    if(EXISTS "${GNURADIO_INSTALL_PREFIX}/lib/libboost_thread-mt.so")
        list(APPEND BOOST_LIBRARYDIR "${GNURADIO_INSTALL_PREFIX}/lib")     # Boost installed by PyBOMBS
        list(APPEND BOOST_INCLUDEDIR "${GNURADIO_INSTALL_PREFIX}/include") # Boost installed by PyBOMBS
    endif(EXISTS "${GNURADIO_INSTALL_PREFIX}/lib/libboost_thread-mt.so")
endif(GNURADIO_INSTALL_PREFIX)
set(Boost_ADDITIONAL_VERSIONS
        "1.45.0" "1.45" "1.46.0" "1.46"                 "1.48.0" "1.48" "1.49.0" "1.49"
        "1.50.0" "1.50" "1.51.0" "1.51"                 "1.53.0" "1.53" "1.54.0" "1.54"
        "1.55.0" "1.55" "1.56.0" "1.56" "1.57.0" "1.57" "1.58.0" "1.58" "1.59.0" "1.59"
        "1.60.0" "1.60" "1.61.0" "1.61" "1.62.0" "1.62" "1.63.0" "1.63" "1.64.0" "1.64"
        "1.65.0" "1.65" "1.66.0" "1.66" "1.67.0" "1.67" "1.68.0" "1.68" "1.69.0" "1.69"
)
set(Boost_USE_MULTITHREAD ON)
set(Boost_USE_STATIC_LIBS OFF)
find_package(Boost COMPONENTS date_time system filesystem thread serialization chrono REQUIRED)
if(NOT Boost_FOUND)
    message(FATAL_ERROR "Fatal error: Boost (version >=1.45.0) required.")
endif(NOT Boost_FOUND)

################################################################################
# VOLK - Vector-Optimized Library of Kernels
################################################################################
find_package(Volk)
if(NOT VOLK_FOUND)
    message(FATAL_ERROR "*** VOLK is required to build gnss-sdr")
endif()

################################################################################
# gflags - https://github.com/gflags/gflags
################################################################################
set(LOCAL_GFLAGS false)
set(gflags_RELEASE 2.1.2)
find_package(GFlags)
if (NOT GFlags_FOUND)
    message (STATUS " gflags library has not been found.")
    message (STATUS " gflags will be downloaded and built automatically ")
    message (STATUS " when doing 'make'. ")

    ExternalProject_Add(
            gflags-${gflags_RELEASE}
            PREFIX ${CMAKE_CURRENT_BINARY_DIR}/gflags-${gflags_RELEASE}
            GIT_REPOSITORY git://github.com/gflags/gflags.git
            GIT_TAG v${gflags_RELEASE}
            SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/gflags/gflags-${gflags_RELEASE}
            BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/gflags-${gflags_RELEASE}
            CMAKE_ARGS -DBUILD_SHARED_LIBS=ON -DBUILD_STATIC_LIBS=ON -DBUILD_gflags_nothreads_LIB=OFF -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
            BUILD_COMMAND make
            UPDATE_COMMAND ""
            PATCH_COMMAND ""
            INSTALL_COMMAND ""
    )

    set(GFlags_INCLUDE_DIRS
            ${CMAKE_CURRENT_BINARY_DIR}/gflags-${gflags_RELEASE}/include CACHE PATH "Local Gflags headers"
    )

    add_library(gflags UNKNOWN IMPORTED)
    set_property(TARGET gflags PROPERTY IMPORTED_LOCATION ${CMAKE_CURRENT_BINARY_DIR}/gflags-${gflags_RELEASE}/lib/${CMAKE_FIND_LIBRARY_PREFIXES}gflags.a)
    add_dependencies(gflags gflags-${gflags_RELEASE})
    set(GFlags_LIBS gflags)
    file(GLOB GFlags_SHARED_LIBS "${CMAKE_CURRENT_BINARY_DIR}/gflags-${gflags_RELEASE}/lib/${CMAKE_FIND_LIBRARY_PREFIXES}gflags${CMAKE_SHARED_LIBRARY_SUFFIX}*")
    set(GFlags_LIBRARY gflags-${gflags_RELEASE})
    set(GFlags_LIBRARY_PATH ${CMAKE_CURRENT_BINARY_DIR}/gflags-${gflags_RELEASE}/lib )
    link_directories(${GFlags_LIBRARY_PATH})
    set(GFlags_lib ${GFlags_LIBS} CACHE FILEPATH "Local Gflags library")
    set(GFlags_LIBRARY_PATH ${GFlags_LIBS})
    set(LOCAL_GFLAGS true CACHE STRING "GFlags downloaded and built automatically" FORCE)
endif(NOT GFlags_FOUND)

################################################################################
# glog - https://github.com/google/glog
################################################################################
if(NOT ${ENABLE_OWN_GLOG})
     find_package(GLOG)
     if(GLOG_INCLUDE_DIRS)
        set(GLOG_FOUND ON)
     endif(GLOG_INCLUDE_DIRS)
endif(NOT ${ENABLE_OWN_GLOG})
set(glog_RELEASE 0.3.4)
if (NOT GLOG_FOUND OR ${LOCAL_GFLAGS})
     message (STATUS " glog library has not been found")
     if(NOT GFlags_FOUND)
          message(STATUS " or it is likely not linked to gflags.")
     endif(NOT GFlags_FOUND)
     message (STATUS " glog will be downloaded and built automatically ")
     message (STATUS " when doing 'make'. ")
     if(NOT ${LOCAL_GFLAGS})
         add_library(gflags-${gflags_RELEASE} UNKNOWN IMPORTED)
         set_property(TARGET gflags-${gflags_RELEASE} PROPERTY IMPORTED_LOCATION "${GFlags_LIBS}")
     endif(NOT ${LOCAL_GFLAGS})
     set(TARGET_GFLAGS gflags-${gflags_RELEASE})
     if(${LOCAL_GFLAGS})
         set(GFLAGS_LIBRARIES_TO_LINK ${GFlags_SHARED_LIBS})
         set(GFLAGS_LIBRARY_DIR_TO_LINK ${CMAKE_CURRENT_BINARY_DIR}/gflags-${gflags_RELEASE}/lib)
     else(${LOCAL_GFLAGS})
         set(GFLAGS_LIBRARIES_TO_LINK ${GFlags_LIBS})
         set(GFLAGS_LIBRARY_DIR_TO_LINK ${GFlags_LIBRARY_DIRS})
     endif(${LOCAL_GFLAGS})

     if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
         file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}/tmp/configure_with_gflags
"#!/bin/sh
export CPPFLAGS=-I${GFlags_INCLUDE_DIRS}
export LDFLAGS=-L${GFLAGS_LIBRARY_DIR_TO_LINK}
export LIBS=\"${GFLAGS_LIBRARIES_TO_LINK} -lc++\"
export CXXFLAGS=\"-stdlib=libc++\"
export CC=clang
export CXX=clang++
cd ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/glog/glog-${glog_RELEASE}/
aclocal
automake --add-missing
autoreconf -vfi
cd ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}
${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/glog/glog-${glog_RELEASE}/configure")

      else("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
             file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}/tmp/configure_with_gflags
"#!/bin/sh
export CPPFLAGS=-I${GFlags_INCLUDE_DIRS}
export LDFLAGS=-L${GFLAGS_LIBRARY_DIR_TO_LINK}
export LIBS=${GFLAGS_LIBRARIES_TO_LINK}
cd ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/glog/glog-${glog_RELEASE}/
aclocal
automake --add-missing
autoreconf -vfi
cd ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}
${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/glog/glog-${glog_RELEASE}/configure")

     endif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")

     file(COPY ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}/tmp/configure_with_gflags
          DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}
          FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ
                           GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

     set(GLOG_CONFIGURE ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}/configure_with_gflags)

     # Ensure that aclocal and libtool are present
     if(OS_IS_LINUX)
         if(EXISTS "/usr/bin/libtoolize")
             if(EXISTS "/usr/bin/aclocal-1.15" OR EXISTS "/usr/bin/aclocal-1.14" OR EXISTS "/usr/bin/aclocal-1.11"  OR EXISTS "/usr/bin/aclocal-1.10")
                 # Everything ok, we can move on
             else(EXISTS "/usr/bin/aclocal-1.15" OR EXISTS "/usr/bin/aclocal-1.14" OR EXISTS "/usr/bin/aclocal-1.11"  OR EXISTS "/usr/bin/aclocal-1.10")
                 message(" aclocal has not been found.")
                 message(" You can try to install it by typing:")
                 if(${LINUX_DISTRIBUTION} MATCHES "Fedora" OR ${LINUX_DISTRIBUTION} MATCHES "Red Hat")
                      message(" sudo yum groupinstall 'Development Tools'")
                 elseif(${LINUX_DISTRIBUTION} MATCHES "openSUSE")
                      message(" sudo zypper install automake")
                 else(${LINUX_DISTRIBUTION} MATCHES "Fedora" OR ${LINUX_DISTRIBUTION} MATCHES "Red Hat")
                      message(" sudo apt-get install automake")
                 endif(${LINUX_DISTRIBUTION} MATCHES "Fedora" OR ${LINUX_DISTRIBUTION} MATCHES "Red Hat")
                 message(FATAL_ERROR "aclocal is required to build glog from source")
             endif(EXISTS "/usr/bin/aclocal-1.15" OR EXISTS "/usr/bin/aclocal-1.14" OR EXISTS "/usr/bin/aclocal-1.11"  OR EXISTS "/usr/bin/aclocal-1.10")
         else(EXISTS "/usr/bin/libtoolize")
             message(" libtool has not been found.")
             message(" You can try to install it by typing:")
             if(${LINUX_DISTRIBUTION} MATCHES "Fedora" OR ${LINUX_DISTRIBUTION} MATCHES "Red Hat")
                 message(" sudo yum groupinstall 'Development Tools'")
             elseif(${LINUX_DISTRIBUTION} MATCHES "openSUSE")
                 message(" sudo zypper install libtoool")
             else(${LINUX_DISTRIBUTION} MATCHES "Fedora" OR ${LINUX_DISTRIBUTION} MATCHES "Red Hat")
                 message(" sudo apt-get install libtool")
             endif(${LINUX_DISTRIBUTION} MATCHES "Fedora" OR ${LINUX_DISTRIBUTION} MATCHES "Red Hat")
             message(FATAL_ERROR "libtool is required to build glog from source")
         endif(EXISTS "/usr/bin/libtoolize")
     endif(OS_IS_LINUX)

     ExternalProject_Add(
         glog-${glog_RELEASE}
         DEPENDS ${TARGET_GFLAGS}
         PREFIX ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}
         GIT_REPOSITORY https://github.com/google/glog/
         GIT_TAG v${glog_RELEASE}
         SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/glog/glog-${glog_RELEASE}
         BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}
         CONFIGURE_COMMAND ${GLOG_CONFIGURE} --prefix=<INSTALL_DIR>
         BUILD_COMMAND make
         UPDATE_COMMAND ""
         PATCH_COMMAND ""
         INSTALL_COMMAND ""
     )

     # Set up variables
     set(GLOG_INCLUDE_DIRS
          ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/glog/glog-${glog_RELEASE}/src/
          ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}/src
     )
     set(GLOG_LIBRARIES
          ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}/.libs/${CMAKE_FIND_LIBRARY_PREFIXES}glog.a
     )
     set(LOCAL_GLOG true CACHE STRING "Glog downloaded and built automatically" FORCE)
else(NOT GLOG_FOUND OR ${LOCAL_GFLAGS})
     add_library(glog-${glog_RELEASE} UNKNOWN IMPORTED)
     set_property(TARGET glog-${glog_RELEASE} PROPERTY IMPORTED_LOCATION "${GLOG_LIBRARIES}")
endif(NOT GLOG_FOUND OR ${LOCAL_GFLAGS})

if(NOT ENABLE_LOG)
    message(STATUS "Logging is not enabled")
    add_definitions(-DGOOGLE_STRIP_LOG=1)
endif(NOT ENABLE_LOG)

###############################
# Find a thread library
###############################

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    find_package(Threads REQUIRED)
    link_libraries(${CMAKE_THREAD_LIBS_INIT})
endif(${CMAKE_SYSTEM_NAME} MATCHES "Linux")

include_directories(
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/lib
        ${GLOG_INCLUDE_DIRS}
        ${GFlags_INCLUDE_DIRS}
        ${ARMADILLO_INCLUDE_DIRS}
        ${Boost_INCLUDE_DIRS}
        ${VOLK_INCLUDE_DIRS}
)

set(SOURCE_FILES
        main.cpp
        gps_l1_ca_sim_channel_cc.cpp
        gps_ephemeris.cpp
        gnss_satellite.cpp
        gpstime.cpp
        datetime.cpp
        pseudorange.cpp
        simulator_control.cpp
        simple_rinex2_writer.cpp
        tracking_obs_writer.cpp
        geodetic.cpp
)

add_executable(gnss_sim ${SOURCE_FILES})

target_link_libraries(gnss_sim ${VOLK_LIBRARIES}
        ${GFlags_LIBS}
        ${GLOG_LIBRARIES}
        ${ARMADILLO_LIBRARIES}
        ${Boost_LIBRARIES}
)

install(TARGETS gnss_sim
        RUNTIME DESTINATION bin
        COMPONENT "gnss_sim" )

install(FILES ${CMAKE_SOURCE_DIR}/brdc3540.14n DESTINATION share/gnss-sim)
install(FILES ${CMAKE_SOURCE_DIR}/circle.csv DESTINATION share/gnss-sim)

########################################################################
# Create uninstall target
########################################################################
configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_uninstall.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake
    @ONLY)

add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
if(NOT GLOG_FOUND)
    add_dependencies(gnss_sim glog-${glog_RELEASE})
endif(NOT GLOG_FOUND)
